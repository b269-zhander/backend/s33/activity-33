
console.log("Activity 33");




// Data
fetch("https://jsonplaceholder.typicode.com/posts/")

.then((response) => response.json())
.then((json) => console.log(json));




// Title of every item

fetch("https://jsonplaceholder.typicode.com/posts/")
.then((response) => response.json())
.then((json) => {
    const titles = json.map(post => post.title);
    console.log(titles);
});






// A single to do list item
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(json));





// Title and message
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => {
    json.forEach((item) => {
        console.log(`Title: ${item.title}, Status: ${item.completed}`);
    });
});




// PUT, update a list
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'PUT',
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		userId: 1,
		title: "updated post",
		completed: false
	})

})
.then((response) => response.json())
.then((json) => console.table(json));





// PATCH, change data structure
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'PATCH',
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		
		title: "updated post",
		description: "a patch update",
		status:"Okay",
		completed: 20230330,
		userId: 1
	})

})
.then((response) => response.json())
.then((json) => console.table(json))




// Delete
fetch("https://jsonplaceholder.typicode.com/posts/3", {
	method: 'DELETE'

})